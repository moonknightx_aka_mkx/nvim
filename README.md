To change Caps to Escape 
Go to /etc/X11/xorg.conf.d/00-keyboard.conf
Type the following Option "XkbOptions" "caps:escape"
So the whole file will look like this

```Section 
"InputClass"
        Identifier "system-keyboard"
        MatchIsKeyboard "on"
        Option "XkbLayout" "us"
        Option "XkbVariant" "dvorak"
        Option "XkbOptions" "caps:escape"
EndSection
```
