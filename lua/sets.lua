vim.cmd("set expandtab")
vim.cmd("set tabstop=4")
vim.cmd("set softtabstop=4")
vim.cmd("set shiftwidth=4")
vim.cmd ("set clipboard+=unnamedplus")
vim.cmd ("set termguicolors")
vim.cmd ("set nohlsearch")
vim.cmd ("set encoding=utf-8")

vim.wo.number = true
vim.wo.relativenumber = true
vim.wo.wrap = false
vim.wo.linebreak = true
vim.wo.list = false
vim.o.cursorline = true

vim.opt.smartindent = true
vim.opt.swapfile = false -- or value = true
vim.opt.backup = false -- or value = true
vim.opt.undofile = true
vim.opt.incsearch = true
vim.opt.termguicolors = true
vim.opt.scrolloff = 8
vim.opt.signcolumn = "yes"
vim.opt.updatetime = 60
vim.opt.colorcolumn = "96"

