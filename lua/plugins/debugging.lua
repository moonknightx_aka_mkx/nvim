return {
	"mfussenegger/nvim-dap",
	dependencies = {
		"rcarriga/nvim-dap-ui",
		"leoluz/nvim-dap-go",
		"mfussenegger/nvim-dap-python",
        "NicholasMata/nvim-dap-cs",
		"theHamsta/nvim-dap-virtual-text",
		"nvim-neotest/neotest",
		"nvim-neotest/nvim-nio",
	},

	config = function()
		local dap = require("dap")
		local dapui = require("dapui")

		require("dapui").setup()
		require("dap-go").setup()
		-- require("dap-python").setup()
		require("dap-python").setup("/usr/bin/python3")
		require("dap-cs").setup()
		require("nvim-dap-virtual-text").setup()

		dap.listeners.before.attach.dapui_config = function()
			dapui.open()
		end
		dap.listeners.before.launch.dapui_config = function()
			dapui.open()
		end
		dap.listeners.before.event_terminated.dapui_config = function()
			dapui.close()
		end
		dap.listeners.before.event_exited.dapui_config = function()
			dapui.close()
		end

		-- vim.keymap.set("n", "<leader>dt", dap.toggle_breakpoint, {})
		-- vim.keymap.set("n", "<leader>dc", dap.continue, {})
		vim.keymap.set("n", "<leader>tc", function()
			if vim.bo.filetype == "python" then
				require("dap-python").test_class()
			end
		end)
		vim.keymap.set("n", "<leader>tm", function()
			if vim.bo.filetype == "python" then
				require("dap-python").test_method()
			end
		end)

		vim.keymap.set("n", "<leader>bb", "<cmd>lua require'dap'.toggle_breakpoint()<cr>")
		vim.keymap.set(
			"n",
			"<leader>bc",
			"<cmd>lua require'dap'.set_breakpoint(vim.fn.input('Breakpoint condition: '))<cr>"
		)
		vim.keymap.set(
			"n",
			"<leader>bl",
			"<cmd>lua require'dap'.set_breakpoint(nil, nil, vim.fn.input('Log point message: '))<cr>"
		)
		vim.keymap.set("n", "<leader>br", "<cmd>lua require'dap'.clear_breakpoints()<cr>")
		vim.keymap.set("n", "<leader>ba", "<cmd>Telescope dap list_breakpoints<cr>")
		vim.keymap.set("n", "<leader>dc", "<cmd>lua require'dap'.continue()<cr>")
		vim.keymap.set("n", "<leader>dj", "<cmd>lua require'dap'.step_over()<cr>")
		vim.keymap.set("n", "<leader>dk", "<cmd>lua require'dap'.step_into()<cr>")
		vim.keymap.set("n", "<leader>do", "<cmd>lua require'dap'.step_out()<cr>")
		vim.keymap.set("n", "<leader>dd", function()
			require("dap").disconnect()
			require("dapui").close()
		end)
		vim.keymap.set("n", "<leader>dt", function()
			require("dap").terminate()
			require("dapui").close()
		end)
		vim.keymap.set("n", "<leader>dr", "<cmd>lua require'dap'.repl.toggle()<cr>")
		vim.keymap.set("n", "<leader>dl", "<cmd>lua require'dap'.run_last()<cr>")
		vim.keymap.set("n", "<leader>di", function()
			require("dap.ui.widgets").hover()
		end)
		vim.keymap.set("n", "<leader>d?", function()
			local widgets = require("dap.ui.widgets")
			widgets.centered_float(widgets.scopes)
		end)
		vim.keymap.set("n", "<leader>df", "<cmd>Telescope dap frames<cr>")
		vim.keymap.set("n", "<leader>dh", "<cmd>Telescope dap commands<cr>")
		vim.keymap.set("n", "<leader>de", function()
			require("telescope.builtin").diagnostics({ default_text = ":E:" })
		end)
	end,
}
