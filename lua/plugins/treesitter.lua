return{
    {
        "nvim-treesitter/nvim-treesitter",
        build = ":TSUpdate",
        dependencies = { 'nvim-treesitter/playground', 'windwp/nvim-ts-autotag' },
        config = function()
            local config = require("nvim-treesitter.configs")
            config.setup({
                auto_install = true,
                highlight = { enable = true },
                indent = { enable = true },
                autotag = {enable = true},
                ignore_install = { "htmldjango" },
            })
        end
    }
}
