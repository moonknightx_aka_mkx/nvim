return {
    "nvimdev/indentmini.nvim",
    config = function()
        require("indentmini").setup() -- use default config
        -- NOTE: Colors are applied automatically based on user-defined highlight groups.
        -- NOTE There is no default value.
        -- vim.cmd.highlight("IndentLine guifg=#04b5ff")
        vim.cmd.highlight("IndentLine guifg=#393B4D")
        -- NOTE Current indent line highlight
        -- vim.cmd.highlight("IndentLineCurrent guifg=#18ef01")
        vim.cmd.highlight("IndentLineCurrent guifg=#502BD2")
    end,
}
