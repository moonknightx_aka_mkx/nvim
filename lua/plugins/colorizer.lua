-- return {
--     "norcalli/nvim-colorizer.lua",
-- 	config = function()
-- 		require("colorizer").setup()
-- 	end,
-- }

-- return {
--     "norcalli/nvim-colorizer.lua",
-- 	config = function()
-- 		require("colorizer").setup({'*';})
-- 	end,
-- }

-- return {
--     "chrisbra/Colorizer",
-- 	config = function()
--         vim.keymap.set("n", "<Leader>ca", ":ColorToggle<CR>", {})
-- 	end,
-- }

-- ===========================================================================================

return {
    "brenoprata10/nvim-highlight-colors",
	config = function()
        require("nvim-highlight-colors").setup({})
	end,
}

-- ===========================================================================================

-- return {
--     "norcalli/nvim-colorizer.lua",
--     config = function()
--         DEFAULT_OPTIONS = {
--             RGB      = true;         -- #RGB hex codes
--             RRGGBB   = true;         -- #RRGGBB hex codes
--             names    = true;         -- "Name" codes like Blue
--             RRGGBBAA = true;        -- #RRGGBBAA hex codes
--             rgb_fn   = true;        -- CSS rgb() and rgba() functions
--             hsl_fn   = true;        -- CSS hsl() and hsla() functions
--             css      = true;        -- Enable all CSS features: rgb_fn, hsl_fn, names, RGB, RRGGBB
--             css_fn   = false;        -- Enable all CSS *functions*: rgb_fn, hsl_fn
--             -- Available modes: foreground, background
--             mode     = 'background'; -- Set the display mode.
--         }
--         -- Attaches to every FileType mode
--         require 'colorizer'.setup()
--
--         -- Attach to certain Filetypes, add special configuration for `html`
--         -- Use `background` for everything else.
--         require 'colorizer'.setup {
--             'css';
--             'javascript';
--             html = {
--                 mode = 'foreground';
--             }
--         }
--
--         -- Use the `default_options` as the second parameter, which uses
--         -- `foreground` for every mode. This is the inverse of the previous
--         -- setup configuration.
--         require 'colorizer'.setup({
--             'css';
--             'javascript';
--             html = { mode = 'background' };
--         }, { mode = 'foreground' })
--
--         -- Use the `default_options` as the second parameter, which uses
--         -- `foreground` for every mode. This is the inverse of the previous
--         -- setup configuration.
--         require 'colorizer'.setup {
--             '*'; -- Highlight all files, but customize some others.
--             css = { rgb_fn = true; }; -- Enable parsing rgb(...) functions in css.
--             html = { names = false; } -- Disable parsing "names" like Blue or Gray
--         }
--
--         -- Exclude some filetypes from highlighting by using `!`
--         require 'colorizer'.setup {
--             '*'; -- Highlight all files, but customize some others.
--             '!vim'; -- Exclude vim from highlighting.
--             -- Exclusion Only makes sense if '*' is specified!
--         }
--     end,
-- }

-- ===========================================================================================

-- return {
--     "NvChad/nvim-colorizer.lua",
--     config = function()
--         require("colorizer").setup{
--             require("colorizer").setup {
--                 filetypes = { "*" },
--                 user_default_options = {
--                     RGB = true, -- #RGB hex codes
--                     RRGGBB = true, -- #RRGGBB hex codes
--                     names = true, -- "Name" codes like Blue or blue
--                     RRGGBBAA = true, -- #RRGGBBAA hex codes
--                     AARRGGBB = true, -- 0xAARRGGBB hex codes
--                     rgb_fn = true, -- CSS rgb() and rgba() functions
--                     hsl_fn = true, -- CSS hsl() and hsla() functions
--                     css = true, -- Enable all CSS features: rgb_fn, hsl_fn, names, RGB, RRGGBB
--                     css_fn = true, -- Enable all CSS *functions*: rgb_fn, hsl_fn
--                     -- Available modes for `mode`: foreground, background,  virtualtext
--                     mode = "background", -- Set the display mode.
--                     -- Available methods are false / true / "normal" / "lsp" / "both"
--                     -- True is same as normal
--                     tailwind = true, -- Enable tailwind colors
--                     -- parsers can contain values used in |user_default_options|
--                     sass = { enable = true, parsers = { "css" }, }, -- Enable sass colors
--                     virtualtext = "■",
--                     -- update color values even if buffer is not focused
--                     -- example use: cmp_menu, cmp_docs
--                     always_update = true
--                 },
--                 -- all the sub-options of filetypes apply to buftypes
--                 buftypes = {},
--             }
--         }
--     end
-- }

-- ===========================================================================================

