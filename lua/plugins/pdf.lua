return {
    "makerj/vim-pdf",
    setup = function()
        require("vim-pdf").setup()
    end,
}
