return {
    "nvimtools/none-ls.nvim",
    depndencies = {
        "nvimtools/none-ls-extras.nvim"
    },
    config = function()
        local null_ls = require("null-ls")
        null_ls.setup({
            sources = {
                null_ls.builtins.formatting.stylua,
                null_ls.builtins.formatting.prettier,
                null_ls.builtins.diagnostics.rubocop,
                null_ls.builtins.formatting.rubocop,
                null_ls.builtins.formatting.black,
                null_ls.builtins.formatting.isort,
                null_ls.builtins.diagnostics.mypy,
                null_ls.builtins.diagnostics.pylint,
                null_ls.builtins.formatting.djlint,
                null_ls.builtins.diagnostics.djlint,
                null_ls.builtins.diagnostics.markdownlint,
                null_ls.builtins.formatting.markdownlint,
                null_ls.builtins.diagnostics.yamllint,
                null_ls.builtins.formatting.shellharden,
                null_ls.builtins.formatting.shfmt,
                null_ls.builtins.formatting.sql_formatter,
				null_ls.builtins.formatting.csharpier,
				null_ls.builtins.formatting.clang_format,
                -- null_ls.builtins.diagnostics.eslint_d,
                -- null_ls.builtins.diagnostics.ruff,
            },
        })

        vim.keymap.set("n", "<leader>gf", vim.lsp.buf.format, {})
    end,
}
