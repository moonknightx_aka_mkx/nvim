return {
    "ellisonleao/carbon-now.nvim",
    lazy = true,
    cmd = "CarbonNow",
    ---@param opts cn.ConfigSchema
    opts = {
        theme = "catppuccin",
        font_family = "FireCode Nerd Font"
    },
    vim.keymap.set("v", "<leader>cn", ":CarbonNow<CR>", { silent = true })
}
