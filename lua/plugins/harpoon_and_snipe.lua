return {

    "ThePrimeagen/harpoon",
    branch = "harpoon2",
    dependencies = { "nvim-lua/plenary.nvim" },
    config = function ()
        local harpoon = require("harpoon")

        -- REQUIRED
        harpoon:setup()
        -- REQUIRED

        vim.keymap.set("n", "<leader>a", function() harpoon:list():append() end)
        vim.keymap.set("n", "<C-e>", function() harpoon.ui:toggle_quick_menu(harpoon:list()) end)

        vim.keymap.set("n", "<C-h>", function() harpoon:list():select(1) end)
        vim.keymap.set("n", "<C-t>", function() harpoon:list():select(2) end)
        vim.keymap.set("n", "<C-n>", function() harpoon:list():select(3) end)
        vim.keymap.set("n", "<C-s>", function() harpoon:list():select(4) end)

        -- Toggle previous & next buffers stored within Harpoon list
        vim.keymap.set("n", "<C-S-P>", function() harpoon:list():prev() end)
        vim.keymap.set("n", "<C-S-N>", function() harpoon:list():next() end)
    end

}

-- =============================================================================================

-- return {
-- 	-- "linkarzu/snipe.nvim",
-- 	"leath-dub/snipe.nvim",
-- 	config = function()
-- 		local snipe = require("snipe")
-- 		snipe.setup({
-- 			hints = {
-- 				-- Charaters to use for hints
-- 				-- make sure they don't collide with the navigation keymaps
-- 				-- If you remove `j` and `k` from below, you can navigate in the plugin
-- 				-- dictionary = "sadflewcmpghio",
-- 				dictionary = "asfghl;wertyuiop",
-- 			},
-- 			navigate = {
-- 				cancel_snipe = "q",
-- 			},
-- 			sort = "default",
-- 		})
-- 		vim.keymap.set("n", "gba", snipe.create_buffer_menu_toggler({ max_path_width = 2 }), { desc = "[P]Snipe" })
-- 	end,
-- }
