vim.g.mapleader = " "

-- vim.keymap.set('n', '<leader>ex', ':Ex<CR>', {})

-- General keymaps
-- vim.keymap.set("i", "jk", "<ESC>") -- exit insert mode with jk 
vim.keymap.set("i", "ii", "<ESC>") -- exit insert mode with jk 

-- ==========================================================================================

-- Remap 'h' to 'h' (left)
vim.api.nvim_set_keymap('n', 'h', 'h', { noremap = true, silent = true })
vim.api.nvim_set_keymap('v', 'h', 'h', { noremap = true, silent = true })

-- Remap 'j' to 't' (down)
vim.api.nvim_set_keymap('n', 't', 'j', { noremap = true, silent = true })
vim.api.nvim_set_keymap('v', 't', 'j', { noremap = true, silent = true })

-- Remap 'k' to 'n' (up)
vim.api.nvim_set_keymap('n', 'n', 'k', { noremap = true, silent = true })
vim.api.nvim_set_keymap('v', 'n', 'k', { noremap = true, silent = true })

-- Remap 'l' to 's' (right)
vim.api.nvim_set_keymap('n', 's', 'l', { noremap = true, silent = true })
vim.api.nvim_set_keymap('v', 's', 'l', { noremap = true, silent = true })

-- Remap 'n' to 'm' (repeat last search)
vim.api.nvim_set_keymap('n', 'm', 'n', { noremap = true, silent = true })
vim.api.nvim_set_keymap('n', 'M', 'N', { noremap = true, silent = true })
vim.api.nvim_set_keymap('v', 'm', 'n', { noremap = true, silent = true })
vim.api.nvim_set_keymap('v', 'M', 'N', { noremap = true, silent = true })

-- ==========================================================================================

-- vim.api.nvim_set_keymap('n', 'h', '<Left>', { noremap = true })
-- vim.api.nvim_set_keymap('n', 't', '<Down>', { noremap = true })
-- vim.api.nvim_set_keymap('n', 'n', '<Up>', { noremap = true })
-- vim.api.nvim_set_keymap('n', 's', '<Right>', { noremap = true })
--
-- vim.api.nvim_set_keymap('v', 'h', '<Left>', { noremap = true })
-- vim.api.nvim_set_keymap('v', 't', '<Down>', { noremap = true })
-- vim.api.nvim_set_keymap('v', 'n', '<Up>', { noremap = true })
-- vim.api.nvim_set_keymap('v', 's', '<Right>', { noremap = true })

-- ==========================================================================================

vim.keymap.set("n", "<leader>wq", ":wq<CR>") -- save and quit
vim.keymap.set("n", "<leader>qq", ":q!<CR>") -- quit without saving
vim.keymap.set("n", "<leader>ww", ":w<CR>") -- save
vim.keymap.set("n", "<leader>so", ":so<CR>") -- save

vim.keymap.set("n", "gx", ":!open <c-r><c-a><CR>") -- open URL under cursor

-- Split window management
vim.keymap.set("n", "<leader>sv", "<C-w>v") -- split window vertically
vim.keymap.set("n", "<leader>sh", "<C-w>s") -- split window horizontally
vim.keymap.set("n", "<leader>se", "<C-w>=") -- make split windows equal width
vim.keymap.set("n", "<leader>sx", ":close<CR>") -- close split window
vim.keymap.set("n", "<leader>sj", "<C-w>-") -- make split window height shorter
vim.keymap.set("n", "<leader>sk", "<C-w>+") -- make split windows height taller
vim.keymap.set("n", "<leader>sl", "<C-w>>5") -- make split windows width bigger 
vim.keymap.set("n", "<leader>sh", "<C-w><5") -- make split windows width smaller
--
-- Tab management
vim.keymap.set("n", "<leader>to", ":tabnew<CR>") -- open a new tab
vim.keymap.set("n", "<leader>tx", ":tabclose<CR>") -- close a tab
vim.keymap.set("n", "<leader>tn", ":tabn<CR>") -- next tab
vim.keymap.set("n", "<leader>tp", ":tabp<CR>") -- previous tab

-- -- Diff keymaps
-- vim.keymap.set("n", "<leader>cc", ":diffput<CR>") -- put diff from current to other during diff
-- vim.keymap.set("n", "<leader>cj", ":diffget 1<CR>") -- get diff from left (local) during merge
-- vim.keymap.set("n", "<leader>ck", ":diffget 3<CR>") -- get diff from right (remote) during merge
-- vim.keymap.set("n", "<leader>cn", "]c") -- next diff hunk
-- vim.keymap.set("n", "<leader>cp", "[c") -- previous diff hunk
--
-- -- Quickfix keymaps
-- vim.keymap.set("n", "<leader>qo", ":copen<CR>") -- open quickfix list
-- vim.keymap.set("n", "<leader>qf", ":cfirst<CR>") -- jump to first quickfix list item
-- vim.keymap.set("n", "<leader>qn", ":cnext<CR>") -- jump to next quickfix list item
-- vim.keymap.set("n", "<leader>qp", ":cprev<CR>") -- jump to prev quickfix list item
-- vim.keymap.set("n", "<leader>ql", ":clast<CR>") -- jump to last quickfix list item
-- vim.keymap.set("n", "<leader>qc", ":cclose<CR>") -- close quickfix list
--
-- -- Vim-maximizer
-- vim.keymap.set("n", "<leader>sm", ":MaximizerToggle<CR>") -- toggle maximize tab
--
-- -- Nvim-tree
-- vim.keymap.set("n", "<leader>ee", ":NvimTreeToggle<CR>") -- toggle file explorer
-- vim.keymap.set("n", "<leader>er", ":NvimTreeFocus<CR>") -- toggle focus to file explorer
-- vim.keymap.set("n", "<leader>ef", ":NvimTreeFindFile<CR>") -- find file in file explorer
--
-- -- Git-blame
-- vim.keymap.set("n", "<leader>gb", ":GitBlameToggle<CR>") -- toggle git blame
--
-- -- Vim REST Console
-- vim.keymap.set("n", "<leader>xr", ":call VrcQuery()<CR>") -- Run REST query
--
-- -- Filetype-specific keymaps (these can be done in the ftplugin directory instead if you prefer)
-- vim.keymap.set("n", '<leader>go', function()
--   if vim.bo.filetype == 'python' then
--     vim.api.nvim_command('PyrightOrganizeImports')
--   end
-- end)
